package ru.t1.ktubaltseva.tm.api.repository.dto;

import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {

}
