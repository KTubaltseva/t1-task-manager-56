package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.model.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.service.model.ISessionService;
import ru.t1.ktubaltseva.tm.model.Session;


@Service
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
