package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.model.IRepository;
import ru.t1.ktubaltseva.tm.api.service.model.IService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.*;


@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;
    
    @NotNull
    protected abstract R getRepository();

    @NotNull
    @Override
    public M add(@Nullable final M model) throws EntityNotFoundException, SqlDataException {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable final Collection<M> models) throws SqlDataException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels = new ArrayList<>();
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final M model : models) {
                @NotNull final M resultModel = modelRepository.add(model);
                resultModels.add(resultModel);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            modelRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findOneById(id);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        @Nullable List<M> resultModels;
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultModels = modelRepository.findAll();
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @Nullable List<M> resultModels;
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultModels = modelRepository.findAll(comparator);
        } finally {
            entityManager.close();
        }
        return resultModels;
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultModel = modelRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize() throws AbstractException {
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        int result = 0;
        try {
            result = modelRepository.getSize();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void remove(@Nullable final M model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            modelRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            modelRepository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        clear();
        return add(models);
    }

    @NotNull
    public M update(
            @Nullable final M model
    ) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        @NotNull final R modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        @Nullable M resultModel;
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

}
