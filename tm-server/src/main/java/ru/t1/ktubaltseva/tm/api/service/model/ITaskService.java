package ru.t1.ktubaltseva.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    ) throws AbstractException;

}
