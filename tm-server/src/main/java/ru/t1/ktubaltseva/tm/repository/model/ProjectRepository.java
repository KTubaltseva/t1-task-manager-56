package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.api.repository.model.IProjectRepository;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.Comparator;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedWBSRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    protected Class<Project> getClazz() {
        return Project.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

}
