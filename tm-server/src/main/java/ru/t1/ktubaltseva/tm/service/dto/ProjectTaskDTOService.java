package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;

import java.util.List;

@Service
public class ProjectTaskDTOService implements IProjectTaskDTOService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Override
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        try {
            projectService.findOneById(userId, projectId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        @Nullable TaskDTO resultTask;
        try {
            resultTask = taskService.findOneById(userId, taskId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        resultTask.setProjectId(projectId);
        resultTask = taskService.update(userId, resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        try {
            projectService.findOneById(userId, projectId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        @Nullable TaskDTO resultTask;
        try {
            resultTask = taskService.findOneById(userId, taskId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        resultTask.setProjectId(null);
        resultTask = taskService.update(userId, resultTask);
        return resultTask;
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        try {
            projectService.findOneById(userId, projectId);
        } catch (@NotNull final EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        taskService.removeAllByProjectId(userId, projectId);
        projectService.removeById(userId, projectId);
    }

    @Override
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final List<ProjectDTO> projects = projectService.findAll(userId);
        for (@NotNull final ProjectDTO project : projects) {
            taskService.removeAllByProjectId(userId, project.getId());
        }
        projectService.clear(userId);
    }

}
