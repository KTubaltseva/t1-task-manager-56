package ru.t1.ktubaltseva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IUserOwnedWBSDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}
