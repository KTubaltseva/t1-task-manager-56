package ru.t1.ktubaltseva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedWBSRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}
