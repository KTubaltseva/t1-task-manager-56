package ru.t1.ktubaltseva.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.model.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.model.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.repository.model.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.model.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.model.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.EmailAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.RoleEmptyException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.security.NoSuchAlgorithmException;


@Service
public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @NotNull
    private IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    private ISessionRepository getSessionRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @Nullable User resultModel;
        @NotNull final IUserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (isEmailExists(login)) throw new EmailAlreadyExistsException(email);
        @Nullable User resultModel;
        @NotNull final IUserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password), email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @Nullable User resultModel;
        @NotNull final IUserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            resultModel = modelRepository.create(login, HashUtil.salt(propertyService, password), role);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser;
        @NotNull final IUserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultUser = modelRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultUser;
        @NotNull final IUserRepository modelRepository = getRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            resultUser = modelRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        try {
            findByLogin(login);
        } catch (UserNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        try {
            findByEmail(email);
        } catch (UserNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    public void remove(@Nullable final User model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserRepository modelRepository = getRepository();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        @NotNull final EntityManager entityManager = modelRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(model.getId());
            projectRepository.clear(model.getId());
            sessionRepository.clear(model.getId());
            modelRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultModel = findByLogin(login);
        remove(resultModel);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultModel = findByEmail(email);
        remove(resultModel);
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User resultUser = findOneById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @Override
    public @NotNull User update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable String lastName
    ) throws AbstractException {
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return update(user);
    }

}
