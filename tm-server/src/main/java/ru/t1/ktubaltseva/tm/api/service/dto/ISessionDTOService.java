package ru.t1.ktubaltseva.tm.api.service.dto;

import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {
}
