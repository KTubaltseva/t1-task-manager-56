package ru.t1.ktubaltseva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;


@Service
public final class TaskDTOService extends AbstractUserOwnedWBSDTOService<TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    @NotNull
    @Override
    protected ITaskDTORepository getRepository() {
        return context.getBean(ITaskDTORepository.class);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<TaskDTO> resultModel;
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            resultModel = repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @NotNull
    @Override
    public void removeAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final ITaskDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
    }

}
