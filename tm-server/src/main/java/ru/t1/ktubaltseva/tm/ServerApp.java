package ru.t1.ktubaltseva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ktubaltseva.tm.component.Bootstrap;
import ru.t1.ktubaltseva.tm.configuration.ServerConfig;

public class ServerApp {

    public static void main(@Nullable final String[] args) throws Exception {
        @NotNull final ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ServerConfig.class);
        @NotNull final Bootstrap bootstrap = applicationContext.getBean(Bootstrap.class);
        bootstrap.start();
    }

}