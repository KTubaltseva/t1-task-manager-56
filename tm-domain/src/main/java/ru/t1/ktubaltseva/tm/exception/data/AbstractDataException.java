package ru.t1.ktubaltseva.tm.exception.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public abstract class AbstractDataException extends AbstractException {

    public AbstractDataException() {
    }

    public AbstractDataException(@NotNull final String message) {
        super(message);
    }

    public AbstractDataException(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractDataException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractDataException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
