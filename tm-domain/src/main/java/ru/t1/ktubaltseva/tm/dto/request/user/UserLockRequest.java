package ru.t1.ktubaltseva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(@Nullable final String token) {
        super(token);
    }

    public UserLockRequest(@Nullable final String token, @Nullable final String login) {
        super(token);
        this.login = login;
    }

}
