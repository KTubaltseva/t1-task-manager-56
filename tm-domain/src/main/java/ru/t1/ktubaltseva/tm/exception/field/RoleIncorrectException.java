package ru.t1.ktubaltseva.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public class RoleIncorrectException extends AbstractFieldException {

    public RoleIncorrectException() {
        super("Error! Role is incorrect...");
    }

    public RoleIncorrectException(@NotNull final String value) {
        super("Error! This role value \"" + value + "\" is incorrect...");
    }

}