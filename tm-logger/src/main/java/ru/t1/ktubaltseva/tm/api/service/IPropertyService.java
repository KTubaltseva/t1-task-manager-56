package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    Integer getMongoPort();

    @NotNull
    String getMongoHost();

    @NotNull
    String getMongoDBName();

}
