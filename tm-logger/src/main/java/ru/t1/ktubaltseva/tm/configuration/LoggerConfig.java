package ru.t1.ktubaltseva.tm.configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;

import javax.jms.ConnectionFactory;

import static ru.t1.ktubaltseva.tm.constant.LoggerConstant.URL;

@Configuration
@ComponentScan("ru.t1.ktubaltseva.tm")
public class LoggerConfig {

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @Bean
    public ConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(URL);
    }

    @Bean
    public MongoClient mongoClient() {
        return new MongoClient(propertyService.getMongoHost(), propertyService.getMongoPort());
    }

    @Bean
    public MongoDatabase mongoDatabase(@NotNull final MongoClient mongoClient) {
        return mongoClient.getDatabase(propertyService.getMongoDBName());
    }

}
