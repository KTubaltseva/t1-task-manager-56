package ru.t1.ktubaltseva.tm.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    private static final String MONGO_PORT_KEY = "mongo.port";

    @NotNull
    private static final String MONGO_PORT_DEFAULT = "27017";

    @NotNull
    private static final String MONGO_HOST_KEY = "mongo.host";

    @NotNull
    private static final String MONGO_HOST_DEFAULT = "127.0.0.1";

    @NotNull
    private static final String MONGO_DB_NAME_KEY = "mongo.db";

    @NotNull
    private static final String MONGO_DB_NAME_DEFAULT = "tm";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    @PostConstruct
    public void init() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    public String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    public @NotNull Integer getMongoPort() {
        return getIntegerValue(MONGO_PORT_KEY, MONGO_PORT_DEFAULT);
    }

    @Override
    public @NotNull String getMongoHost() {
        return getStringValue(MONGO_HOST_KEY, MONGO_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getMongoDBName() {
        return getStringValue(MONGO_DB_NAME_KEY, MONGO_DB_NAME_DEFAULT);
    }

    @NotNull
    public Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    public String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    public String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

}
