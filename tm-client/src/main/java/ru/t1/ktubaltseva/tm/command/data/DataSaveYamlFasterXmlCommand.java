package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.data.DataSaveYamlFasterXmlRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

@Component
public final class DataSaveYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-yaml-faster-xml";

    @NotNull
    private final String DESC = "Save data to yaml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SAVE YAML DATA]");
        @NotNull final DataSaveYamlFasterXmlRequest request = new DataSaveYamlFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataYamlFasterXml(request);
    }

}
