package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectCompleteByIdRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-complete-by-id";

    @NotNull
    private final String DESC = "Complete project by Id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken(), id);
        getProjectEndpoint().completeProjectById(request);
    }

}
