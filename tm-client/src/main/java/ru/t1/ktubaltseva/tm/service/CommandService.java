package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.repository.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.service.ICommandService;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

@Service
public final class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) throws CommandNotSupportedException {
        if (argument == null || argument.isEmpty()) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandRepository.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        return abstractCommand;
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) throws CommandNotSupportedException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandRepository.getCommandByName(name);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        return abstractCommand;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getSystemCommands() {
        return commandRepository.getSystemCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommandsWithArgument() {
        return commandRepository.getTerminalCommandsWithArgument();
    }

}
